#!/bin/bash
#
# Run "git-crypt unlock" with in-repo gpg key, for cloned repo and all submodules in the repo.
# Assuming this repo and all submodule repos are encrypted with your same gpg keyid. 
# If a file name is given at the command line, decrypt this repo using the given symmetric key.
#
# Author: sfeng@stanford.edu
# Date: Sun Apr 17 18:03:13 PDT 2016

function abort() {
  [ -n "$@" ] && { echo "abort: $@"; exit 1; }
}

function skip() {
  [ -n "$@" ] && echo "skip: $@";
}

function verify_gpg_passphrase() {
  echo "Enter the passphrase that will be used to decrypt all in-repo gpg encrypted files, including submodule repos."
  echo "Enter passphrase:"
  read -s passphrase
  [ -z "$passphrase" ] && abort "Empty passphrase"

  # Try to encrypt a test file at repo top level with the passphrase, if not successful, exit
  userids=$(gpg --list-keys | grep pub | grep -oE "/(\w+) " | tr '/' ' ')
  foundkey=1
  echo "test" > /tmp/gpg_test.in
  for i in $userids
  do
    echo $passphrase | \
    gpg -q --sign --local-user $i --passphrase-fd 0 --output /dev/null --yes /tmp/gpg_test.in && \
    echo "The correct passphrase was entered for $i" && foundkey=0 && break
  done
} 

function do_unlock() {
  keyfile=$1
  if [ ! -z $keyfile ];
  then
    git-crypt unlock $keyfile
  else
    expect 2> /dev/null <<EOF
spawn git-crypt unlock
expect "Enter passphrase:"
send "$passphrase\r";
expect eof
EOF
  fi
}

# MAIN
keyfile=${1:-''}
if [ -z $keyfile ];
then
  verify_gpg_passphrase
  [ $foundkey -ne 0 ] && abort "Unable to decrypt."
else
  [ ! -f $keyfile ] && abort "unable to read $keyfile."
fi

# unlock current repo
do_unlock $keyfile

# unlock submodules
modules=$(git submodule status)
[ -z "$modules" ] && abort "No submodules"

git submodule status | while read i
do
  m=$(echo $i | cut -d' ' -f2)
  if echo $i | grep ^- > /dev/null 2>&1 ;
  then
    skip "Skipping $m; Please update module with: git submodule init $m && git submodule update $m."
    continue
  else
    ( 
      echo
      echo
      echo "==== Unlocking $m ===="
      echo "Checking out $i master branch"
      cd $m
      git checkout master
      do_unlock $keyfile
      echo "==== End of $m ===="
    )
  fi
done
